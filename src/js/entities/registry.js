import BaseClass from '../classes/base.js';

var EntityRegistry = BaseClass.extend({
	init: function() {
		this.entities = [];
	},
	add: function(entity) {
		this.entities.push(entity);
	},
	remove: function(entity) {
		var i = this.entities.indexOf(entity);
		if (i !== -1) {
			this.entities.splice(i, 1);
		}
	},
	exists: function(entity) {
		var i, len = this.entities.length;
		for(i = 0; i < len; i++) {
			if (this.entities[i] === entity) {
				return true;
			}
		}
		return false;
	},
	every: function(cb) {
		var i, len = this.entities.length;
		for(i = 0; i < len; i++) {
			cb(this.entities[i]);
		}
	}
});

var registry = new EntityRegistry();

export default registry;