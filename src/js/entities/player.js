import EntityRegistry from './registry.js';
import kontra from '../lib/kontra.js';

export default function Player(x, y) {
	var halfWidth = kontra.canvas.width / 2;
	var result = kontra.sprite({
		x: x,
		y: y,
		ddx: 0.01,
		ddy: 0,

		color: 'red',
		radius: 20,
		render: function() {
			var c = this.context;
			c.fillStyle = this.color;

			c.beginPath();
			c.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
			c.fill();
		},
		update: function(dt) {
			this.advance(dt);
			if (this.x > halfWidth) {
				this.ddx = -0.01;
			} else if (this.x < halfWidth) {
				this.ddx = 0.01;
			}
		}
	});
	EntityRegistry.add(result);
	return result;
};