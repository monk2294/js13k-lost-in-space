import kontra from './lib/kontra.js';

kontra.init();

import registry from './entities/registry.js';
import Player from './entities/player.js';

function update(e) {e.update();}
function render(e) {e.render();}

registry.add(Player(20, 20));

var loop = kontra.gameLoop({
	fps: 60,
	clearCanvas: true,
	update: function(dt) {
		registry.every(update);
	},
	render: function() {
		registry.every(render);
	}
});
loop.start();